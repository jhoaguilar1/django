from django.core.paginator import Paginator
from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse



from .models import Docentes, Grupos, Jornadas, Materias, Roles

def index(request):
    return render(request, 'polls/index.html')

def docentes(request):
    q = Docentes.objects.all()
    contexto = {'data': q}
    return render(request, 'polls/docentes.html', contexto)

def registrarDocente(request):
    return render(request, 'polls/docentes_registrar.html')


def guardarDocente(request):
    q = Docentes(
        nombre_docente = request.POST["nombre_docente"],
        correo_docente = request.POST["correo_docente"],
        telefono_docente = request.POST["telefono_docente"],
        idmateria = request.POST["idmateria"],
        
    )
    q.save()
    return HttpResponseRedirect(reverse('polls:docentes'))

def eliminarDocentes(request, iddocente):
    #obtener objeto a través de ID
    q = Docentes.objects.get(pk = iddocente)
    q.delete()
    contexto = { 'docente': q.nombre_docente }
    return render(request, 'polls/docentes_eliminar.html', contexto)

def materia(request):
    q = Materias.objects.all()
    contexto = {'data': q}
    return render(request, 'polls/materias.html', contexto)

def registrarMateria(request):
    return render(request, 'polls/materias_registrar.html')

def guardarMateria(request):
    q = Materias(
        nombre_materia = request.POST["nombre_materia"]
        
    )
    q.save()
    return HttpResponseRedirect(reverse('polls:materia'))

def editarMateria(request,idmateria):
    q = Materias.objects.get(pk = idmateria)
    contexto = { 'data': q }
    return render(request, 'polls/materias_editar.html', contexto)

def actualizarMateria(request):
    #Consulto por <<id>> la empresa y obtengo un objeto....
    e = Materias.objects.get(pk = request.POST["idmateria"])

    #actualizo los atributos del objeto, por los que viene del form
    e.nombre_materia = request.POST["nombre_materia"]
    
    
    #actualizamos el objeto en BD.
    e.save()
    return HttpResponseRedirect(reverse('polls:materia'))

def buscarMateria(request):
    q = Materias.objects.filter(nombre_materia__icontains = request.POST["dato"])
    contexto = {'data': q}
    return render(request, 'polls/materias.html', contexto)

def eliminarMateria(request, idmateria):
    #obtener objeto a través de ID
    q = Materias.objects.get(pk = idmateria)
    q.delete()
    contexto = { 'materia': q.nombre_materia }
    return render(request, 'polls/materias_eliminar.html', contexto)

def jornadas(request):
    q = Jornadas.objects.all()
    contexto = {'data': q}
    return render(request, 'polls/jornadas.html', contexto)

def registrarJornadas(request):
    return render(request, 'polls/jornadas_registrar.html')


def guardarJornada(request):
    q = Jornadas(
        nombre_jornada = request.POST["nombre_jornada"]    
    )
    q.save()
    return HttpResponseRedirect(reverse('polls:jornada'))

def eliminarjornada(request, idjornada):
    #obtener objeto a través de ID
    q = Jornadas.objects.get(pk = idjornada)
    q.delete()
    contexto = { 'jornada': q.nombre_jornada }
    return render(request, 'polls/jornadas_eliminar.html', contexto)

def buscarjornada(request):
    q = Jornadas.objects.filter(nombre_jornada__icontains = request.POST["dato"])
    contexto = {'data': q}
    return render(request, 'polls/jornadas.html', contexto)

def editarJornada(request,idjornada):
    q = Jornadas.objects.get(pk = idjornada)
    contexto = { 'data': q }
    return render(request, 'polls/jornadas_editar.html', contexto)

def actualizarJornada(request):
    #Consulto por <<id>> la empresa y obtengo un objeto....
    e = Jornadas.objects.get(pk = request.POST["idjornada"])

    #actualizo los atributos del objeto, por los que viene del form
    e.nombre_jornada = request.POST["nombre_jornada"]
    
    
    #actualizamos el objeto en BD.
    e.save()
    return HttpResponseRedirect(reverse('polls:jornada'))

def roles(request):
    q = Roles.objects.all()
    contexto = {'data': q}
    return render(request, 'polls/roles.html', contexto)

def registrarRol(request):
    return render(request, 'polls/roles_registrar.html')


def guardarRol(request):
    q = Roles(
        nombre_rol = request.POST["nombre_rol"],
        permisos_rol = request.POST["permisos_rol"]     
    )
    q.save()
    return HttpResponseRedirect(reverse('polls:rol'))


def eliminarRol(request, idrol):
    #obtener objeto a través de ID
    q = Roles.objects.get(pk = idrol)
    q.delete()
    contexto = { 'rol': q.nombre_rol }
    return render(request, 'polls/roles_eliminar.html', contexto)

def buscarRol(request):
    q = Roles.objects.filter(nombre_rol__icontains = request.POST["dato"])
    contexto = {'data': q}
    return render(request, 'polls/roles.html', contexto)

def editarRol(request,idrol):
    q = Roles.objects.get(pk = idrol)
    contexto = { 'data': q }
    return render(request, 'polls/roles_editar.html', contexto)

def actualizarRol(request):
    #Consulto por <<id>> la empresa y obtengo un objeto....
    e = Roles.objects.get(pk = request.POST["idrol"])

    #actualizo los atributos del objeto, por los que viene del form
    e.nombre_rol = request.POST["nombre_rol"]
    e.permisos_rol = request.POST["permisos_rol"]
    
    
    #actualizamos el objeto en BD.
    e.save()
    return HttpResponseRedirect(reverse('polls:rol'))

def grupos(request):
    q = Grupos.objects.all()
    contexto = {'data': q}
    return render(request, 'polls/grupos.html', contexto)

def registrarGrupo(request):
    return render(request, 'polls/grupos_registrar.html')


def guardarGrupo(request):
    q = Grupos(
        nombre_grupo = request.POST["nombre_grupo"],
            
    )
    q.save()
    return HttpResponseRedirect(reverse('polls:grupo'))


def eliminarGrupo(request, idgrupo):
    #obtener objeto a través de ID
    q = Grupos.objects.get(pk = idgrupo)
    q.delete()
    contexto = { 'grupo': q.nombre_grupo }
    return render(request, 'polls/grupos_eliminar.html', contexto)

def buscarGrupo(request):
    q = Grupos.objects.filter(nombre_grupo__icontains = request.POST["dato"])
    contexto = {'data': q}
    return render(request, 'polls/grupos.html', contexto)

def editarGrupo(request,idgrupo):
    q = Grupos.objects.get(pk = idgrupo)
    contexto = { 'data': q }
    return render(request, 'polls/grupos_editar.html', contexto)

def actualizarGrupo(request):
    #Consulto por <<id>> la empresa y obtengo un objeto....
    e = Grupos.objects.get(pk = request.POST["idgrupo"])

    #actualizo los atributos del objeto, por los que viene del form
    e.nombre_grupo = request.POST["nombre_grupo"]
    
    
    
    #actualizamos el objeto en BD.
    e.save()
    return HttpResponseRedirect(reverse('polls:grupo'))