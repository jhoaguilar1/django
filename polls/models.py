# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

class PollsGrados(models.Model):
    idgrado = models.AutoField(primary_key=True)
    nombre_grado = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'polls_grados'


class PollsGrupos(models.Model):
    idgrupo = models.AutoField(primary_key=True)
    nombre_grupo = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'polls_grupos'


class PollsJornadas(models.Model):
    idjornada = models.AutoField(primary_key=True)
    nombre_jornada = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'polls_jornadas'

class PollsMaterias(models.Model):
    idmateria = models.AutoField(primary_key=True)
    nombre_materia = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'polls_materias'


class PollsRoles(models.Model):
    idrol = models.AutoField(primary_key=True)
    nombre_rol = models.CharField(db_column='nombre_Rol', max_length=250)  # Field name made lowercase.
    permisos_rol = models.CharField(db_column='permisos_Rol', max_length=250)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'polls_roles'


class Roles(models.Model):
    idrol = models.AutoField(primary_key=True)
    nombre_rol = models.CharField(db_column='nombre_Rol', max_length=250)  # Field name made lowercase.
    permisos_rol = models.CharField(db_column='permisos_Rol', max_length=250)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'roles'


class Tiposdedocumento(models.Model):
    idtipodocumento = models.AutoField(primary_key=True)
    nombre_tipodocumento = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'tiposdedocumento'
class Asistencias(models.Model):
    idasistencia = models.AutoField(primary_key=True)
    nombre_asistencia = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'asistencias'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'
class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'
class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'
class Grados(models.Model):
    idgrado = models.AutoField(primary_key=True)
    nombre_grado = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'grados'


class Grupos(models.Model):
    idgrupo = models.AutoField(primary_key=True)
    nombre_grupo = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'grupos'
class Jornadas(models.Model):
    idjornada = models.AutoField(primary_key=True)
    nombre_jornada = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'jornadas'
class Materias(models.Model):
    idmateria = models.AutoField(primary_key=True)
    nombre_materia = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'materias'
class PollsAsistencias(models.Model):
    idasistencia = models.AutoField(primary_key=True)
    nombre_asistencia = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'polls_asistencias'




class Acudientes(models.Model):
    idacudiente = models.AutoField(primary_key=True)
    nombre_acudiente = models.CharField(max_length=250)
    documento_docente = models.CharField(max_length=250)
    correo_acudiente = models.CharField(max_length=250)
    telefono_acudiente = models.CharField(max_length=250)
    idtipodocumento = models.ForeignKey('Tiposdedocumento', models.DO_NOTHING, db_column='idtipodocumento')
    idrol = models.ForeignKey('Roles', models.DO_NOTHING, db_column='idrol')

    class Meta:
        managed = False
        db_table = 'acudientes'





class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)





class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'





class Docentes(models.Model):
    iddocente = models.AutoField(primary_key=True)
    nombre_docente = models.CharField(db_column='nombre_Docente', max_length=250)  # Field name made lowercase.
    documento_docente = models.CharField(max_length=250)
    telefono_docente = models.CharField(db_column='telefono_Docente', max_length=250)  # Field name made lowercase.
    correo_docente = models.CharField(max_length=250)
    idmateria = models.ForeignKey('Materias', models.DO_NOTHING, db_column='idmateria')
    idrol = models.ForeignKey('Roles', models.DO_NOTHING, db_column='idrol')
    idtipodocumento = models.ForeignKey('Tiposdedocumento', models.DO_NOTHING, db_column='idtipodocumento')

    class Meta:
        managed = False
        db_table = 'docentes'


class Estudiantes(models.Model):
    idestudiante = models.AutoField(primary_key=True)
    nombre_estudiante = models.CharField(max_length=250)
    fecha_nacimiento = models.DateField()
    telefono_alumno = models.CharField(max_length=250)
    documento_estudiante = models.CharField(max_length=250)
    idacudiente = models.ForeignKey(Acudientes, models.DO_NOTHING, db_column='idacudiente')
    idgrado = models.ForeignKey('Grados', models.DO_NOTHING, db_column='idgrado')
    idgrupo = models.ForeignKey('Grupos', models.DO_NOTHING, db_column='idgrupo')
    idjornada = models.ForeignKey('Jornadas', models.DO_NOTHING, db_column='idjornada')
    idasistencia = models.ForeignKey(Asistencias, models.DO_NOTHING, db_column='idasistencia')
    idrol = models.ForeignKey('Roles', models.DO_NOTHING, db_column='idrol')
    idtipodocumento = models.ForeignKey('Tiposdedocumento', models.DO_NOTHING, db_column='idtipodocumento')

    class Meta:
        managed = False
        db_table = 'estudiantes'








class Login(models.Model):
    idlogin = models.AutoField(primary_key=True)
    usuario = models.CharField(max_length=250)
    contrasena = models.CharField(max_length=250)
    idrol = models.ForeignKey('Roles', models.DO_NOTHING, db_column='idrol')

    class Meta:
        managed = False
        db_table = 'login'





class PollsAcudientes(models.Model):
    idacudiente = models.AutoField(primary_key=True)
    nombre_acudiente = models.CharField(max_length=250)
    telefono_acudiente = models.CharField(max_length=250)
    correo_acudiente = models.CharField(max_length=250)
    genero_acudiente = models.CharField(max_length=250)
    idrol = models.ForeignKey('PollsRoles', models.DO_NOTHING, db_column='idrol')

    class Meta:
        managed = False
        db_table = 'polls_acudientes'





class PollsDocentes(models.Model):
    iddocente = models.AutoField(primary_key=True)
    nombre_docente = models.CharField(db_column='nombre_Docente', max_length=250)  # Field name made lowercase.
    correo_docente = models.CharField(db_column='correo_Docente', max_length=250)  # Field name made lowercase.
    telefono_docente = models.CharField(db_column='telefono_Docente', max_length=30)  # Field name made lowercase.
    idmateria = models.IntegerField()
    idrol = models.ForeignKey('PollsRoles', models.DO_NOTHING, db_column='idrol')

    class Meta:
        managed = False
        db_table = 'polls_docentes'


class PollsEstudiantes(models.Model):
    idestudiante = models.AutoField(primary_key=True)
    nombre_estudiante = models.CharField(max_length=250)
    fecha_nacimiento = models.DateField()
    telefono_alumno = models.CharField(max_length=30)
    idacudiente = models.ForeignKey(PollsAcudientes, models.DO_NOTHING, db_column='idacudiente')
    idasistencia = models.ForeignKey(PollsAsistencias, models.DO_NOTHING, db_column='idasistencia')
    idgrado = models.ForeignKey('PollsGrados', models.DO_NOTHING, db_column='idgrado')
    idgrupo = models.ForeignKey('PollsGrupos', models.DO_NOTHING, db_column='idgrupo')
    idjornada = models.ForeignKey('PollsJornadas', models.DO_NOTHING, db_column='idjornada')
    idrol = models.ForeignKey('PollsRoles', models.DO_NOTHING, db_column='idrol')

    class Meta:
        managed = False
        db_table = 'polls_estudiantes'





class PollsLogin(models.Model):
    idlogin = models.AutoField(primary_key=True)
    usuario = models.CharField(max_length=250)
    contrasena = models.CharField(max_length=250)
    idrol = models.ForeignKey('PollsRoles', models.DO_NOTHING, db_column='idrol')

    class Meta:
        managed = False
        db_table = 'polls_login'


