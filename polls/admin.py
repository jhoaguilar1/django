from django.contrib import admin

# Register your models here.
def auto_register(model):
    #Get all fields from model, but exclude autocreated reverse relations
    
    #field_list = [f.name for f in model._meta.get_fields() if f.auto_created == False]

    #modificación para evitar que los display superen los 10 campos. Jor ------------
    field_list = []
    conteo = 0
    for f in model._meta.get_fields():
        if f.auto_created == False:
            if conteo < 10:
                field_list.append(f.name)
            else:
                break
            conteo += 1
    #fin modificación. Jor -----------------------------------------------------------

    # Dynamically create ModelAdmin class and register it.
    my_admin = type('MyAdmin', (admin.ModelAdmin,), 
                        {'list_display':field_list, 'search_fields': field_list }
                        )
    try:
        admin.site.register(model,my_admin)
    except:# AlreadyRegistered:
        # This model is already registered
        pass
 
 
from django.apps import apps
for model in apps.get_app_config('polls').get_models():
    auto_register(model)
