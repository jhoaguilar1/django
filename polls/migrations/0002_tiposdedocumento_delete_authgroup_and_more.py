# Generated by Django 4.0.3 on 2022-03-08 04:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tiposdedocumento',
            fields=[
                ('idtipodocumento', models.AutoField(primary_key=True, serialize=False)),
                ('nombre_tipodocumento', models.CharField(max_length=250)),
            ],
            options={
                'db_table': 'tiposdedocumento',
                'managed': False,
            },
        ),
        migrations.DeleteModel(
            name='AuthGroup',
        ),
        migrations.DeleteModel(
            name='AuthGroupPermissions',
        ),
        migrations.DeleteModel(
            name='AuthPermission',
        ),
        migrations.DeleteModel(
            name='AuthUser',
        ),
        migrations.DeleteModel(
            name='AuthUserGroups',
        ),
        migrations.DeleteModel(
            name='AuthUserUserPermissions',
        ),
        migrations.DeleteModel(
            name='DjangoAdminLog',
        ),
        migrations.DeleteModel(
            name='DjangoContentType',
        ),
        migrations.DeleteModel(
            name='DjangoMigrations',
        ),
        migrations.DeleteModel(
            name='DjangoSession',
        ),
        migrations.AlterModelOptions(
            name='acudientes',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='asistencias',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='docentes',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='estudiantes',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='grados',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='grupos',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='jornadas',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='login',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='materias',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='roles',
            options={'managed': False},
        ),
    ]
