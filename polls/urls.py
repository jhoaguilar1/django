from django.urls import path

from . import views

app_name = 'polls'
urlpatterns = [
    path('', views.index, name='index'),
    path('docentes/', views.docentes, name='docentes'),
    path('materias/', views.materia, name='materia'),
    path('jornadas/', views.jornadas, name='jornada'),
    path('rol/', views.roles, name='rol'),
    path('grupo/', views.grupos, name='grupo'),
    path('materia_registrar/', views.registrarMateria, name='materia_registrar'),
    path('registrar_docentes/', views.registrarDocente, name='registrar_docentes'),
    path('jornada_registrar/', views.registrarJornadas, name='jornada_registrar'),
    path('rol_registrar/', views.registrarRol, name='rol_registrar'),
    path('grupo_registrar/', views.registrarGrupo, name='grupo_registrar'),
    path('guardar_materia/', views.guardarMateria, name='guardar_materia'),
    path('guardar_docente/', views.guardarDocente, name='guardar_docente'),
    path('guardar_jornada/', views.guardarJornada, name='guardar_jornada'),
    path('guardar_rol/', views.guardarRol, name='guardar_rol'),
    path('guardar_grupo/', views.guardarGrupo, name='guardar_grupo'),
    path('editar_materia/<int:idmateria>', views.editarMateria, name='editar_materia'),
    path('editar_jornada/<int:idjornada>', views.editarJornada, name='editar_jornada'),
    path('editar_rol/<int:idrol>', views.editarRol, name='editar_rol'),
    path('editar_grupo/<int:idgrupo>', views.editarGrupo, name='editar_grupo'),
    path('actualizar_materia/', views.actualizarMateria, name="actualizar_materia"),
    path('actualizar_jornada/', views.actualizarJornada, name="actualizar_jornada"),
    path('actualizar_rol/', views.actualizarRol, name="actualizar_rol"),
    path('actualizar_grupo/', views.actualizarGrupo, name="actualizar_grupo"),
    path('buscar_materia/', views.buscarMateria, name='buscar_materia'),
    path('buscar_jornada/', views.buscarjornada, name='buscar_jornada'),
    path('buscar_rol/', views.buscarRol, name='buscar_rol'),
    path('buscar_grupo/', views.buscarGrupo, name='buscar_grupo'),
    path('eliminar_materia/<int:idmateria>', views.eliminarMateria, name='eliminar_materia'),
    path('eliminar_docente/<int:iddocente>', views.eliminarDocentes, name='eliminar_docente'),
    path('eliminar_jornada/<int:idjornada>', views.eliminarjornada, name='eliminar_jornada'),
    path('eliminar_rol/<int:idrol>', views.eliminarRol, name='eliminar_rol'),
    path('eliminar_grupo/<int:idgrupo>', views.eliminarGrupo, name='eliminar_grupo'),
    
]